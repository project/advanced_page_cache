# Advanced Internal Page Cache

The Advanced Page Cache module allows one to leverage Drupal core's
`page_cache` module to build a contextual cache id.

## Technical implementation

The Advanced Page Cache module makes use of the Service Collector pattern
and allows a service definition to be tagged with the `advanced_page_cache_cid` tag.

The module contains two example modules (`cookie_page_cache` and `ip_page_cache`).

## Requirements

Requires the Drupal `page_cache` module.

## How to use
* Create a custom module
* Create a `.service.yml` definition file
  * Use `class` directive to define a class
  * Use `tags` directive to tag with `advanced_page_cache_cid`
  * Use `arguments` directive to pass the contextual cache id value
* Create a class that implements `AdvancedPageCacheInterface`
  * Implement the method `getAdditionalCacheIdPart` that returns the cache id part

## Resources
- See https://www.drupal.org/docs/8/api/services-and-dependency-injection/service-tags


## Maintainers
George Anderson (geoanders) - https://www.drupal.org/u/geoanders
