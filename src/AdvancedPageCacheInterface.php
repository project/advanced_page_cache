<?php

namespace Drupal\advanced_page_cache;

use Symfony\Component\HttpFoundation\Request;

/**
 * Defines required methods for classes wanting to add a page Cache iD.
 *
 * @ingroup cache
 */
interface AdvancedPageCacheInterface {

  /**
   * Adds a part to the Page Cache ID for the current request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A Request object.
   *
   * @return string
   *   A cid.
   */
  public function getAdditionalCacheIdPart(Request $request): string;

}
