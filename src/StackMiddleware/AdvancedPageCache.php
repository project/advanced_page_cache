<?php

namespace Drupal\advanced_page_cache\StackMiddleware;

use Drupal\advanced_page_cache\AdvancedPageCacheInterface;
use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\Request;

/**
 * Advanced Page Cache class.
 */
class AdvancedPageCache extends PageCache implements AdvancedPageCacheInterface {

  /**
   * Holds an array of cache IDs.
   *
   * @var \Drupal\advanced_page_cache\AdvancedPageCacheInterface[]
   */
  protected $cacheIds = [];

  /**
   * Gets the page cache ID for this request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   *
   * @return string
   *   The cache ID for this request.
   */
  protected function getCacheId(Request $request): string {
    if (!isset($this->cid)) {
      // Default cid parts.
      $cid_parts = [
        $request->getSchemeAndHttpHost() . $request->getRequestUri(),
        $request->getRequestFormat(NULL),
      ];
      $this->cid = implode(':', $cid_parts);

      // Only add additional cid parts if enabled.
      if (\Drupal::config('advanced_page_cache.settings')->get('enabled')) {
        $this->cid .= $this->getAdditionalCacheIdPart($request);
      }
    }

    return $this->cid;
  }

  /**
   * Add a cache id.
   *
   * @param \Drupal\advanced_page_cache\AdvancedPageCacheInterface $cacheId
   *   A service object.
   */
  public function addCacheId(AdvancedPageCacheInterface $cacheId) {
    $this->cacheIds[] = $cacheId;
  }

  /**
   * {@inheritdoc}
   */
  public function getAdditionalCacheIdPart(Request $request): string {
    $cid_parts = [];

    foreach ($this->cacheIds as $cacheId) {
      $cid_parts[] = $cacheId->getAdditionalCacheIdPart($request);
    }

    return implode(':', $cid_parts);
  }

}
