<?php

namespace Drupal\advanced_page_cache;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Advanced Page Cache Service.
 */
class AdvancedPageCacheService {

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Advanced Page Cache constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config.
   */
  public function __construct(ConfigFactoryInterface $config) {
    $this->config = $config->getEditable('advanced_page_cache.settings');
  }

  /**
   * Get configuration.
   *
   * @return \Drupal\Core\Config\Config
   *   Returns the config.
   */
  public function getConfig(): Config {
    return $this->config;
  }

  /**
   * Determine if Advanced Page Cache is enabled.
   *
   * @return bool
   *   Returns the enabled status.
   */
  public function isEnabled(): bool {
    return $this->getConfig()->get('enabled') ?? FALSE;
  }
}
