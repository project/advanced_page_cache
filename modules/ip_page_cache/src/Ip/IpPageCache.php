<?php

namespace Drupal\ip_page_cache\Ip;

use Drupal\advanced_page_cache\AdvancedPageCacheInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 */
class IpPageCache implements AdvancedPageCacheInterface {

  /**
   * The IP address.
   *
   * @var string
   */
  private $ip;

  /**
   * Constructs a IpPageCache object.
   *
   * @param string $ip
   *   The IP address.
   */
  public function __construct(string $ip) {
    $this->ip = $ip;
  }

  /**
   * Adds the IP address to the page cache ID for this request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   *
   * @return string
   *   A cache id part.
   */
  public function getAdditionalCacheIdPart(Request $request): string {
    return $request->getClientIp() ?? '';
  }

}
