<?php

namespace Drupal\cookie_page_cache\Cookie;

use Drupal\advanced_page_cache\AdvancedPageCacheInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 */
class CookiePageCache implements AdvancedPageCacheInterface {

  /**
   * The cookie name/identifier.
   *
   * @var string
   */
  private $cookie;

  /**
   * Constructs a CookiePageCache object.
   *
   * @param string $cookie
   *   The cookie.
   */
  public function __construct(string $cookie) {
    $this->cookie = $cookie;
  }

  /**
   * Adds a cookie value to the page cache ID for this request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   *
   * @return string
   *   A cache id part.
   */
  public function getAdditionalCacheIdPart(Request $request): string {
    return $request->cookies->get($this->cookie);
  }

}
